# Playbooks

This is a project for ansible playbooks. You will find individual playbooks and playbooks that use the roles directory structure.


The ansible playbooks mentioned below use the roles directory structure, which is a way of breaking your code down into more moudlar units that can be 
used in other projects where needed. Each of the mentioned playbooks has a roles path executed which also exist as a project in this Gitlab. You will find a main.yml file in the tasks folder that will complete it's steps before executing the bash script that lives in the associated files folder.   

db-pre-patch.ymlch 
db-apply-patch.yml
db-post-patch.yml


 ───playbooks
 ───roles
    └───Linux
        ├───DB-apply-patch
        │   ├───files
        │   ├───meta
        │   └───tasks
       

───playbooks
 ───roles
    └───Linux        
        ├───DB-post-patch
        │   ├───files
        │   ├───meta
        │   └───tasks

        
───playbooks
 ───roles
    └───Linux        
        └───DB-pre-patch
            ├───files
            ├───meta
            └───tasks
